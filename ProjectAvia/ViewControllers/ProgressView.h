#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProgressView : UIView

+(instancetype)sharedInstance;
-(void)show:(void(^)(void))comletion;
-(void)dismiss:(void(^)(void))completion;

@end

NS_ASSUME_NONNULL_END
