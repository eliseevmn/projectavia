#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TicketsTableViewController : UITableViewController

-(instancetype)initFavoriteTicketsTableViewController;
-(instancetype)initWithTickets:(NSArray *)tickets;

@end

NS_ASSUME_NONNULL_END
