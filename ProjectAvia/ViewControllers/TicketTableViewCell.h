#import <UIKit/UIKit.h>
#import "DataManager.h"
#import "FavoriteTicket+CoreDataClass.h"
#import "FavoriteMapPrice+CoreDataClass.h"

NS_ASSUME_NONNULL_BEGIN

@interface TicketTableViewCell : UITableViewCell

@property (strong, nonatomic) UIImageView *airlineLogo;

@property (strong, nonatomic) Ticket *ticket;
@property (strong, nonatomic) FavoriteTicket *favoriteTicket;
@property (strong, nonatomic) FavoriteMapPrice *favoriteMapPrice;

@end

NS_ASSUME_NONNULL_END
