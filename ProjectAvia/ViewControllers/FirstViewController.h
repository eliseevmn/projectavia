#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FirstViewController : UIPageViewController <UIPageViewControllerDelegate, UIPageViewControllerDataSource>

@end

NS_ASSUME_NONNULL_END
