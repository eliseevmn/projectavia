#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ContentFirstViewController : UIViewController

@property (strong, nonatomic) NSString *contentText;
@property (strong, nonatomic) UIImage *image;
@property (nonatomic) int index;

@end

NS_ASSUME_NONNULL_END
