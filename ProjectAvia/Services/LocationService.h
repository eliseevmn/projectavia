#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#define locationServiceDidUpdateCurrentLocation @"LocationServiceDidUpdateCurrentLocation"

NS_ASSUME_NONNULL_BEGIN

@interface LocationService : NSObject

@end

NS_ASSUME_NONNULL_END
