#import "AppDelegate.h"
#import "TabBarController.h"
#import "NotificationCenter.h"

#define notificationCenter [NotificationCenter sharedInstance]

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
    self.window.rootViewController = [TabBarController new];
    [self.window makeKeyAndVisible];

    [notificationCenter registerService];

    return YES;
}

@end

