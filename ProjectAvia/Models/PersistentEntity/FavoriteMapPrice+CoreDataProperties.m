#import "FavoriteMapPrice+CoreDataProperties.h"

@implementation FavoriteMapPrice (CoreDataProperties)

+ (NSFetchRequest<FavoriteMapPrice *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"FavoriteMapPrice"];
}

@dynamic actual;
@dynamic departure;
@dynamic destination;
@dynamic distance;
@dynamic numberOfChanges;
@dynamic origin;
@dynamic returnDate;
@dynamic value;
@dynamic created;

@end
